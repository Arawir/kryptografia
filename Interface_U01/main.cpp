#include <iostream>

extern "C"
{
    #include "unif01.h"
    #include "ulcg.h"`
    #include "ufile.h"
    #include "bbattery.h"
    #include "util.h"
}

int main()
{
    unif01_Gen *gen;
    gen = ufile_CreateReadBin( "../RandomNumbers/QNGFile1gb.dat", 1000000 );
    ufile_InitReadBin();
    bbattery_SmallCrush( gen );
    ulcg_DeleteGen( gen );
    return 0;
}
