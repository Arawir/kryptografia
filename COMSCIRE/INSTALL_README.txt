ComScire� QNG Software & Device Driver Installation

(1) WINDOWS -- 

To Install Software and Device Driver:
Install the software and device driver before connecting QNG device to your computer. Wait for the Setup programs to autorun. The QNG Setup 3.5.2 installation program will open in a separate window. 

If setup program does not autorun and AutoPlay Window appears, choose �QNG35_Setup.exe� to run installation setup file.

If Setup and AutoPlay does not autorun, open My Computer:
Open the CD drive by double-clicking on its icon. Double-click on �QNG35_Setup.exe� to run installation setup file and install QNG software and device driver.

(2) LINUX --

To Install Software and Device Driver:

Cancel Windows installation autorun.

Open the "Linux" directory on the CD and copy "libqwqng-1.3.5" or extract "libqwqng-1.3.5.tar.bz2" to a directory on your Computer. Open the newly copied directory and read "QWQNG_Linux.pdf" for software and device driver installation instruction.

Prerequisite libraries for libqwqng-1.3.5, LIBUSB-1.0 and LIBFTDI-1.0, are included in "Prereq_libraries" directory.

(3) MAC --

To Install Software and Device Driver:

Cancel Windows installation autorun. Open the "Mac" directory on the CD.

If MAC OS X 10.8 (Mountain Lion) --

Double click on libqwqngSDK.pkg to install the SDK resources. Once installed, precompiled SDK files will be found in the /opt/comscire directory. Additional libraries (Libusb and Libftdi1) will be located in /opt/local directory. 

Earlier MAC OS X Versions--

Open the "Mac" directory on the CD and copy "comscire" directory to a directory on your computer. SDK source and header files are available in the "src" directory and will have to be compiled using your method of choice. Note: your environment will need to be setup in order to compile the necessary libraries. Libusb and Libftdi libraries will have to be installed prior to compiling the libqwqng library.    