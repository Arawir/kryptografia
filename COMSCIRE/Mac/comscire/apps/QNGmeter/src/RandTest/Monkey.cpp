//#include "StdAfx.h"
#include "Monkey.h"
#include "math.h"

CMonkey::CMonkey(void)
{
    CreateChi2Test();

	pow2_32 = pow( 2.0, 32 );
	ResetAll();
}

CMonkey::~CMonkey(void)
{
    delete MetaChi2;
}


// Submits a 32 bit word for OQSO testing
void CMonkey::InsertWord32(uint32_t InWord32)
{
    WordStream >>= 32;
    WordStream |= (uint64_t)InWord32 << 32;

	// Submit as many overlapped words as possible from wordstream
	while ( LetterPointer >= 0 )
	{
		// Word is 20 bits long, overlapped every 5 bits
		CurrentWord = (uint32_t)(WordStream>>LetterPointer);
		CurrentWord &= 0x000fffff;

		if ( !(MonkeyBitmap.CheckWord( CurrentWord )) )
			MissingWords--;

		// Test 2097152 words (or 10485775 bits)
		WordCount++;
		if ( WordCount>=2097152 )
		{
			// Calc current and cumulative z-scores
			totalBlockCount++;

            double UnitZScore = -((double)MissingWords-141909.1945)/294.656;
            MetaChi2->Insert(MissingWords);

			ZScoreTotal += UnitZScore;
            P_Chi2 = MetaChi2->GetPvalue();

			if (totalBlockCount!=0)
				cumulativeZScore = ZScoreTotal / sqrt(totalBlockCount);

			ResetTest();
		}

		LetterPointer -= 5;
	}
	
	LetterPointer += 32;
}

// Resets cumulative and current test
void CMonkey::ResetAll(void)
{
	cumulativeZScore = 0;
	WordStream = 0;
	ZScoreTotal = 0;
	totalBlockCount = 0.;
	LetterPointer = 12;
	P_Chi2 = .5;
    MetaChi2->Reset();

	ResetTest();
}

// Resets current test
void CMonkey::ResetTest(void)
{
	MissingWords = 1048576;
	MonkeyBitmap.Clearmap();
	WordCount = 0;
}

void CMonkey::CreateChi2Test(void) {
    double pTable[] = {
        0.020425106042364360, 0.040666886611789466, 0.060624469736722530, 0.080722874121201882, 0.100616024365302590,
        0.121111557989184160, 0.141316741312114800, 0.161190294260755260, 0.181010551247786590, 0.201336360312275590,
        0.220952088811612490, 0.241618453755012560, 0.261080541994648310, 0.281318385062176210, 0.301101543569135540,
        0.321488496511307680, 0.341181838428444410, 0.361317585259280760, 0.381845068778582600, 0.401397398244069000,
        0.421199812732216570, 0.441203620790318660, 0.461358597855332670, 0.481613369255693700, 0.501915809494334120,
        0.522213267201832700, 0.542453183015864760, 0.561245958515391940, 0.581227859302548170, 0.602312062265134120,
        0.621811898488902280, 0.642273933790631250, 0.661093911748785290, 0.681945109184993740, 0.701057916258531310,
        0.721913004618617960, 0.742031836114077970, 0.761368129712952200, 0.780886988484691740, 0.801352419218223980,
        0.821507434015748990, 0.841146751285022030, 0.860823505673494970, 0.880811378033371730, 0.901066731831903670,
        0.920705072974826730, 0.940520391294565220, 0.960455569212208540, 0.980690646095035980, 1.0
    };

    double boundaryTable[] = {
        141306, 141395, 141452, 141496, 141532, 141564, 141592, 141617, 141640, 141662,
        141682, 141702, 141720, 141738, 141755, 141772, 141788, 141804, 141820, 141835,
        141850, 141865, 141880, 141895, 141910, 141925, 141940, 141954, 141969, 141985,
        142000, 142016, 142031, 142048, 142064, 142082, 142100, 142118, 142137, 142158,
        142180, 142203, 142228, 142256, 142288, 142324, 142368, 142426, 142518, 1e100
    };

    MetaChi2 = new Chi2(50, pTable, true, boundaryTable, false);
}
