 
--------------------------------------------------------------------------------
libqwqng version 1.3.5
--------------------------------------------------------------------------------

General Information
===================

libqwqng-1.3.5 - A library (using libftdi1) which allows access to Comscire 
QNG devices including the popular R2000KU.

libqwqng requires the open source library libFTDI1 installed. 
In addition, libusb 1.0 API is needed by libFTDI1 to access FTDI devices. 

*** WARNING *** LIBUSB by design allows other applications to assume control 
of any USB device including QNG devices. Only one QNG device can be attached 
to any computer. This is a possible security issue of Linux that is out of 
our control. 

The official web site is:
  http://www.comscire.com

Contact at:
  <contact@comscire.com>


The binary libraries of libqwqng-1.3.5 and access libraries, libusb and libftdi1, were precompiled for MAC OS X 10.8 (Mountain Lion). The sources will have to be recompiled for any earlier versions of MAC OS X. 

Installation
============
Double click on package installer and follow the instructions. Once installed, Comscire SDK files will be found in the /opt/comscire directory. The header file will be located in /opt/comscire/include and the binary libraries in /opt/comscire/lib. QNGmeter and other applications will be located in the /opt/comscire/apps directory. Libusb and libftdi binary libraries are located in /opt/local/lib directory and their header files in /opt/local/include.

API reference is found in QWQNG_Linux.pdf under API Reference section on page 9. 

For earlier versions of MAC OS X:

Build and install libusb-1.0, libftdi1 and libqwqng-1.3.5 respectively. Source code for libqwqng-1.3.5 is located in /opt/comscire/src. Prior to installation, your environment will need to be setup in order to compile the necessary libraries. GNU C (gcc) , C++ (g++) compilers are needed. The gcc and g++ compilers can be downloaded and installed through Macports. Libusb and libftdi libraries can be installed through Macports as well. For information on Macports, please visit their website at http://www.macports.org/.


1.   Install the libusb-1.0
--------------------------

Using Macports method:

      Open terminal and run command as root:

      # sudo port install libusb


2.    Install the libftdi1
---------------------------

Using Macports method:

      Open terminal and run command as root:

      # sudo port install libftdi1


3.    Build and install the libqwqng-1.3.5
-------------------------------------------

Build libqwqng libraries using your method of choice. Source code is located in /opt/comscire/src directory. Xcode was used to generate the precompiled libraries for OS X 10.8 found in /opt/comscire/lib.  


4.    QNGmeter and Example Programs
------------------------------------

Source code for QNGmeter is located in /opt/comscire/QNGmeter/src. The latest version of gcc compiler is needed to compile the QNGmeter Console application since the built-in compiler in Xcode 4.6.3 does not support OpenMP. 

Source code for API example programs are located in /opt/comscire/examples/src.

--------------------------------------------------------------------------------
www.comscire.com                     		2013 Quantum World Corporation
--------------------------------------------------------------------------------
