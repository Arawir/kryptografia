// Load QNG control
var xQNG = new ActiveXObject("QWQNG.QNG");

// Open the QNG
//xQNG.Open();

// You must call Reset() to complete hardware initialization.
// When "late binding" the QNG control Reset(), must always be called first.
xQNG.Reset();

// Get random numbers
var Int32 = xQNG.RandInt32;
var Uniform = xQNG.RandUniform;
var Normal = xQNG.RandNormal;

// Show results in popup
WScript.Echo('Int32: ' + Int32.toString() + '\n' +
'Uniform: ' + Uniform.toString() + '\n' +
'Normal: ' + Normal.toString());