# Kryptografia Kwantowa

- https://mega.nz/#F!I2IX2YQS!1msXwZCY_0pw6kBErxc0Vw -- Link do danych z kwantowego generatora.
- http://simul.iro.umontreal.ca/testu01/guideshorttestu01.pdf -- user's guide TestU01.
- http://www-users.math.umn.edu/~garrett/students/reu/pRNGs.pdf -- opis różnych testów losowości z odrobiną teroii.
- http://itcourses.eu/files/cybersec/random_number_generators.pdf?fbclid=IwAR1ztGJB0EntaL-GgNEbHmkPySwlCtBXd2EOxckmcv4JF5qDrQHeDSOS38I -- prezentacaj z fajnymi cytatami, kilkoma definicjami, przykładami PRNG które przechodzą TestU01 i LavaRand.