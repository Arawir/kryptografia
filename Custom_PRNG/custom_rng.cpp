#include <iostream>

extern "C"
{
    #include "unif01.h"
    #include "ulcg.h"
    #include "ufile.h"
    #include "bbattery.h"
    #include "util.h"
}

// Middle Square Weyl Sequence
// https://pthree.org/2018/07/30/middle-square-weyl-sequence-prng/
// x - random output [0,0xffffffff]
// w - Weyl sequence (period 2^64)
// s - non-zero 64-bit odd constant

uint64_t x = 0, w = 0, s = 0xb5ad4eceda1ce2a9;

uint32_t msvs()
{
    x *= x;
    x += ( w += s );
    return x = ( x>>32 ) | ( x<<32 );
}


int main()
{
    unif01_Gen *generator = unif01_CreateExternGenBits( "Middle Square Weyl Sequence", *msvs );
    bbattery_SmallCrush( generator );
    unif01_DeleteExternGenBits( generator );

    return 0;
}
