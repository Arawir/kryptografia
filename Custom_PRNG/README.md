# Dodawanie dowolnych generatorów liczb losowych

TestU01 pozwala na testowanie dowolnych generatorów, pod warunkiem,
że generator jest funkcją w C, nie przyjmującą żadnych argumentów oraz
zwraca wartości typu double w zakresie [0, 1) lub inty w zakresie [0, 2^32 - 1].

Jeśli mamy generator doubli to używmay funkcji:
> unif01_Gen *unif01_CreateExternGen01( char *name, double (*gen01)(void) )

Jeśli mamy generator intów to używamy funkcji:
> unif01_Gen *unif01_CreateExternGenBits( char *name, unsigned int (*genB)(void) )

Więcej informacji na stornie 15 w http://simul.iro.umontreal.ca/testu01/guideshorttestu01.pdf.

W custom_rng.cpp znajduje się przykład z generatorem Middle Square Weyl Sequence, który podobno
przechodzi wszystkie testy z TestU01 (na razie sprawdziłem tylko SmallCrush i Crush).
Opis generatora jest na stronie: https://pthree.org/2018/07/30/middle-square-weyl-sequence-prng/